import { Controller, Get, Req, Res } from '@nestjs/common';
import { AppService } from './app.service';
import { PDFNet } from '@pdftron/pdfnet-node';
import { Data } from './file/data';
const ejs = require('ejs');
const path = require('path');
const fs = require('fs');

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  // @Get('neww')
  // async getPDF(@Res() res): Promise<any> {

  //   const SampleData = {
  //     username: 'Johnn',
  //     address: '25',
  //     sub: '123',
  //     state: 'Califonia'
    
    
  //   }

  //   const redData =  {
  //     "id": "651d1dadff26b8ae44f0e220",
  //     "firstDonation": false,
  //     "typeDdonation": "โลหิต",
  //     "recentlyProblems": false,
  //     "problems": "ไม่มี",
  //     "stopDonate": "ไม่มี",
  //     "contact": [
  //       "email"
  //     ],
  //     "objective": "string",
  //     "status": "not complete",
  //     "userId": "65180b5f8624d006a5c19726",
  //     "createAt": "2023-10-04T08:09:17.008Z",
  //     "updateAt": "2023-10-04T08:09:17.008Z",
  //     "answer": []
  //   }



  //   res.set({
  //     'Content-Type': 'application/pdf',
  //     'Content-Disposition': 'attachment; filename=example.pdf',
  //   });

  //   const buffer = await this.appService.getPDF(redData);

  //   res.end(buffer);
  // }

  // @Get('pdf/download')
  // async downloadPDF(@Res() res): Promise<void> {
  //   const buffer = await this.appService.generatePDF();

  //   res.set({
  //     'Content-Type': 'application/pdf',
  //     'Content-Disposition': 'attachment; filename=example.pdf',
  //     'Content-Length': buffer.length,
  //   });

  //   res.end(buffer);
  // }

  @Get('pdf/downloadtwo')
  async downloadPDFTwo(@Res() res) {
    const MyData = {
      id: '233',
    };

    const data = Data.MyDBXS
    const data2 = Data.MyDBMS

    const html = await ejs.renderFile(
      './src/pdf/tem5.html.ejs',
      { body: data2 },
      { async: true },
    );
    var htmltopdf = require('html-pdf-node');

    let options = { format: 'A4' };

    let file = { content: html };

    // let file = {
    //   content:
    //     'ใบกรอกประวัติผู้เบริจาคโลหิต'+
    //     '<br>หน่วยคลังเลือดและเวชศาสตร์บรอการโลหิต</br>'+
    //     '<br><div> <h3>โรงพยาบาลวงขลานครินทร์ </h3></div></br>'+
    //     '<br> <h3>สำหรับผู้บริจาคโลหิตกรอกข้อมูล </h3></br> <br>(H.N.:&nbsp;.................) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วันที่:&nbsp;...........</br>'+
    //     '<br>ชื่อ:&nbsp;</br><br>วัน/เดือน/ปีเกิด:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;อายุ:&nbsp;ปี&nbsp;&nbsp;หมู่โลหิต:&nbsp;&nbsp;.......</br>',
    // };

    htmltopdf.generatePdf(file, options).then((pdfBuffer: any) => {
      res
        .writeHead(200, {
          'Content-Type': 'application/pdf',
          'Content-Disposition': 'attachment; filename=example2233.pdf',
        })
        .end(pdfBuffer);
    });
  }

  // @Get('convertfromoffice')
  // async convert(@Res() res, @Req() req) {
  //   const { filename } = req.query;

  //   const inputPath = path.resolve(__dirname, `./src/file/${filename}`);
  //   const outputPath = path.resolve(__dirname, `./src/file/${filename}.pdf`);

  //   const convertToPDF = async () => {
  //     const pdfdoc = await PDFNet.PDFDoc.create();
  //     await pdfdoc.initSecurityHandler();
  //     await PDFNet.Convert.toPdf(pdfdoc, inputPath);
  //     pdfdoc.save(outputPath, PDFNet.SDFDoc.SaveOptions.e_linearized);
  //   };

  //   PDFNet.runWithCleanup(convertToPDF)
  //     .then(() => {
  //       fs.readFile(outputPath, (err, data) => {
  //         if (err) {
  //           res.statusCode = 500;
  //           res.end(err);
  //         } else {
  //           res.setHeader('Content-Type', 'application/pdf');
  //           res.end(data);
  //         }
  //       });
  //     })
  //     .catch((err) => {
  //       res.statusCode = 500;
  //       res.end(err);
  //     });
  // }
}
