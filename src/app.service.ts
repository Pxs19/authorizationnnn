import { Injectable, Res } from '@nestjs/common';
import { log } from 'console';
import { resolve } from 'path';
// const PDFDocument = require('pdfkit-table');
import * as fs from 'fs'

import { PDFDocument } from 'pdf-lib'
import fontkit from '@pdf-lib/fontkit'
const { readFile, writeFile } = require('fs/promises');

// const fontkit = require('@pdf-lib/fontkit');

const MyData = {
  id: '233',
};

@Injectable()
export class AppService {
  async createPDF(input, data: any): Promise<any> {



    try {
      let fontBytes = fs.readFileSync('./src/file/NotoSansThai_Condensed-Light.ttf');
      const pdfDoc = await PDFDocument.load(fs.readFileSync(input));
      pdfDoc.registerFontkit(fontkit);
      
      const fieldNames = pdfDoc
        .getForm()
        .getFields()
        .map((f) => f.getName());

      // add font
      console.log(fontBytes);
      const custom =await pdfDoc.embedFont(fontBytes);
      // console.log(custom);

      // const font4 = await pdfDoc.embedFont(fs.readFileSync('./src/file/Kanit-Light.ttf'))
     

      

     

      const form = pdfDoc.getForm();
      
      form.getTextField(fieldNames[0]).setText('');
      form.getTextField(fieldNames[1]).setText(data.createAt);
      form.getTextField(fieldNames[2]).setText('Moren Iggy');
      form.getTextField(fieldNames[3]).setText('-');
      form.getTextField(fieldNames[4]).setText('2023-10-02');
      form.getTextField(fieldNames[5]).setText('25');
      form.getTextField(fieldNames[6]).setText('AB');
      form.getTextField(fieldNames[7]).setText('Hatyai Songkla 90110');
      form.getTextField(fieldNames[8]).setText('');
      form.getTextField(fieldNames[9]).setText('90110');
      form.getTextField(fieldNames[10]).setText('0921451253');
      form.getTextField(fieldNames[11]).setText('Buddhism');
      


      const pdfBytes = await pdfDoc.save();

      return pdfBytes;
    } catch (error) {
      console.error(error);
      throw new Error(`Failed to load PDF file: ${error.message}`);
    }
  }

  async getPDF(data: any): Promise<Buffer> {
    return this.createPDF('./src/file/filled4.pdf', data);
  }

  generatePDFTwo() {
    throw new Error('Method not implemented.');
  }
  getHello(): string {
    return 'Hello World!';
  }

  // async generatePDF(): Promise<Buffer> {
  //   const pdfBuffer: Buffer = await new Promise((resolve) => {
  //     const doc = new PDFDocument({
  //       size: 'A4',
  //       bufferPages: true,
  //     });

  //     doc.text('โรงพยาบาลสงขลานครินทร์', {
  //       column: 2,
  //       align: 'justify',
  //     });

  //     const buffer = [];
  //     doc.on('data', buffer.push.bind(buffer));
  //     doc.on('end', () => {
  //       const data = Buffer.concat(buffer);
  //       resolve(data);
  //     });
  //     doc.end();
  //   });

  //   return pdfBuffer;
  // }

  // async getPDFTwo(@Res() res) {
  //   var htmltopdf = require('html-pdf-node');

  //   let options = { format: 'A4' };
  //   // Example of options with args //
  //   // let options = { format: 'A4', args: ['--no-sandbox', '--disable-setuid-sandbox'] };

  //   let file = { content: '<h1>Welcome to html-pdf-node</h1>' };
  //   // or //
  //   // let file = { url: 'https://example.com' };
  //   htmltopdf.generatePdf(file, options).then((pdfBuffer: any) => {
  //     res
  //       .writeHead(200, {
  //         'Content-Type': 'application/pdf',
  //         'Content-Disposition': 'attachment',
  //       })
  //       .end(pdfBuffer);
  //   });

  //   // const pdfBuffer = await htmltopdf.generatePdf(file, options);
  //   // console.log('PDF Buffer:', pdfBuffer);
  // }
}
