import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Role } from '../entities/role.enum';
import { ROLES_KEY } from '../entities/role.decorator';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {

    // logic for person have access
    // if you have access return true
    // if not access return false
    // automatic return 403

    // =========
    // what is the require role ?
    // reflector = use to read a meta data
    const requireRoles = this.reflector.getAllAndOverride<Role[]>(ROLES_KEY, [
        context.getHandler(),
        context.getClass(),
      ]);

      if(!requireRoles)
        return true

    //   const { user } = context.switchToHttp().getRequest();

    const user = {
        name: "Marius",
        roles: [Role.ADMIN]
    
    
    };
    // does the current user making the request have those required role(s)
    return requireRoles.some((role) => user.roles.includes(role));
  }
}